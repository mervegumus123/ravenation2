using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    public GameObject Bild;
    
    [SerializeField] 
    private string Scenename;

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            Bild.SetActive(true);

            
        }
        
        if (Bild.activeInHierarchy && Input.GetKey(KeyCode.Space))
        {
            SceneManager.LoadScene("Hub");
        }
    }
}
