using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Hub : MonoBehaviour
{
    public void enterKaterblau()
    {
        SceneManager.LoadScene("1_Katerblau");
    }
    
    public void enterWatergate()
    {
        SceneManager.LoadScene("2_Watergate");
    }

    public void enterBerghain()
    {
        SceneManager.LoadScene("3_Berghain");
    }
}
